import React, { Component } from 'react';
import Home from './views/Home';
import './sass/reset.css';
import 'element-theme-default';
import { i18n } from 'element-react'
import locale from 'element-react/src/locale/lang/en'

i18n.use(locale);

export default class App extends Component {
	render() {
		return (
			<Home></Home>
		);
	}
}
