import React, { Component } from 'react';
import '../sass/card.scss';

export default class Card extends Component {
	ownerAvatarStyle = {
		fontSize: this.props.avatarSettings[0] + 'rem',
	};
	ownerJobStyle = {
	}
	render() {
		const {props} = this
		if (props.avatarSettings[2] === true) {
			this.ownerAvatarStyle.marginLeft = props.avatarSettings[1];
			this.ownerAvatarStyle.order = 1;
			this.ownerJobStyle.textAlign = 'right';
		} else {
			this.ownerAvatarStyle.marginRight = props.avatarSettings[1];
		}
		return (
			<span className="card">
				<span className="card__main">
					<span style={this.ownerAvatarStyle} className="card__owner-avatar"><i className={props.ownerInfo[0]}></i></span>
					<div>
						<div className="card__owner-name">{props.ownerInfo[1]}</div>
						<div style={this.ownerJobStyle}>{props.ownerInfo[2]}</div>
					</div>
				</span>
				<span className="card__side">{props.ownerInfo[3]}</span>
			</span>
		);
	}
}
