import React, { Component } from 'react';
import Card from './Card';
import '../sass/worker-info.scss';

export default class WorkerInfo extends Component {
	render() {
		return (
			<div className="worker-info">
				<div className="worker-info__elem1">
					<Card ownerInfo={['far fa-user-circle', 'Worker Name', 'Designation']}
		        	avatarSettings={[4, 16]} />
				</div>
				<div className="worker-info__elem2">
					<div className="asset-id">
						<span className="prop-name">Asset ID:</span>
						<span>#3465</span>
					</div>
					<div className="tog-id">
						<span className="prop-name">Tog ID:</span>
						<span>#3465</span>
					</div>
				</div>
				<div className="worker-info__elem3">
					<Card ownerInfo={['far fa-user-circle', 'Supervisor Name', 'Designation']}
		        	avatarSettings={[3, 8, true]} />
				</div>
			</div>
		);
	}
}
