import React, { Component } from 'react';
import { AutoComplete } from 'element-react';
import { Pagination } from 'element-react';
import Card from './Card';
import '../sass/workers.scss';

export default class Workers extends Component {
	state = {
		restaurants: [
		{ "value": "vue"},
		{ "value": "element"},
		{ "value": "cooking"},
		{ "value": "mint-ui"},
		{ "value": "vuex"},
		{ "value": "vue-router"},
		{ "value": "babel"}
		],
		value: '',
	}
	querySearch(queryString, cb) {
		const { restaurants } = this.state;
		const results = queryString ? restaurants.filter(this.createFilter(queryString)) : restaurants;
		cb(results);
	}
	createFilter(queryString) {
		return (restaurant) => {
			return (restaurant.value.toLowerCase().indexOf(queryString.toLowerCase()) === 0);
		};
	}
	handleSelect(item) {

	}
	render() {
		return (
			<div className="workers">
				<AutoComplete placeholder="Search Workers" value={this.state.value} fetchSuggestions={this.querySearch.bind(this)}
		          onSelect={this.handleSelect.bind(this)} triggerOnFocus={false}
		        ></AutoComplete>
	        	<ul className="list-items grow height-blocked">
	        		<li className="list-item">
	        			<button className="list-item__btn">
	        				<Card ownerInfo={['far fa-user-circle', 'Worker Name', 'Designation', 'ID Number']}
	        				avatarSettings={[2, 8,]}></Card>
	        			</button>
	        		</li>
	        		<li className="list-item">
	        			<button className="list-item__btn">
	        				<Card ownerInfo={['far fa-user-circle', 'Worker Name', 'Designation', 'ID Number']}
	        				avatarSettings={[2, 8,]}></Card>
	        			</button>
	        		</li>
	        		<li className="list-item">
	        			<button className="list-item__btn">
	        				<Card ownerInfo={['far fa-user-circle', 'Worker Name', 'Designation', 'ID Number']}
	        				avatarSettings={[2, 8,]}></Card>
	        			</button>
	        		</li>
	        		<li className="list-item">
	        			<button className="list-item__btn">
	        				<Card ownerInfo={['far fa-user-circle', 'Worker Name', 'Designation', 'ID Number']}
	        				avatarSettings={[2, 8,]}></Card>
	        			</button>
	        		</li>
	        	</ul>
		        <Pagination layout="prev, pager, next" total={100}/>
			</div>
		);
	}
}