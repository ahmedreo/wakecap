import React, { Component } from 'react';
import { Menu } from 'element-react';
import { Button } from 'element-react';
import '../sass/sidebar.scss';

export default class SideBar extends Component {
	render() {
		return (
			<div className="sidebar">
				<Menu mode="vertical" defaultActive="2" className="el-menu-vertical-demo">
			        <Menu.ItemGroup title="Options">
			          <Menu.Item index="1"><i className="el-icon-message"></i>Dashboard</Menu.Item>
			          <Menu.Item index="2"><i className="el-icon-message"></i>Workers</Menu.Item>
			          <Menu.Item index="3"><i className="el-icon-message"></i>Zones</Menu.Item>
			          <Menu.Item index="4"><i className="el-icon-message"></i>Assign Helments</Menu.Item>
			          <Menu.Item index="5"><i className="el-icon-message"></i>Reports</Menu.Item>
			          <Menu.Item index="6"><i className="el-icon-message"></i>Mange Site</Menu.Item>
			          <Menu.Item index="7"><i className="el-icon-message"></i>Settings</Menu.Item>
			        </Menu.ItemGroup>
		      	</Menu>
		      	<Button className="sidebar__footer-btn" type="danger">Emergency</Button>
			</div>
		);
	}
}
