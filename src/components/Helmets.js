import React, { Component } from 'react';
import '../sass/helmets.scss';

export default class Helmets extends Component {
	render() {
		return (
			<div className="helmets">
				<h2 className="helmets__heading">Assigned Helmets</h2>
				<ul className="list-items">
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
					<li className="list-item">
						<div className="list-item__inner">
							<div className="list-item__detail"><i className="list-item__detail__icon fas fa-hat-wizard"></i><span>055687150</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-address-card"></i><span>IN-006</span></div>
							<div className="list-item__detail"><i className="list-item__detail__icon far fa-clock"></i><span>7 mins ago</span></div>
						</div>
					</li>
				</ul>
			</div>
		);
	}
}
