import React, { Component } from 'react';
import { Button } from 'element-react';
import Card from './Card';
import '../sass/worker-activity.scss';

export default class WorkerActivity extends Component {
	render() {
		return (
			<div className="worker-activity">
				<div className="worker-activity__cards">
					<div className="worker-activity__card">
						<Card ownerInfo={['far fa-calendar-times', 'Total Hours Worked', '2,158']} avatarSettings={[2, 12]} />
					</div>
		        	<div className="worker-activity__card">
		        		<Card ownerInfo={['fas fa-map-marker', 'Last Active Zone', 'Zone 2']} avatarSettings={[2, 12]} />
	        		</div>
	        		<Button size="larg" type="info">Full History</Button>
				</div>
				<div className="worker-activity__chart grow width-blocked">
					<img src="/imgs/chart.png" alt=""/>
				</div>
			</div>
		);
	}
}
