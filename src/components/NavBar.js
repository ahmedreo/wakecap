import React, { Component } from 'react';
import { Dropdown } from 'element-react';
import { Button } from 'element-react';
import { Select } from 'element-react';
import '../sass/navbar.scss';

export default class NavBar extends Component {
  state = {
    options: [{
      value: 'Option 1',
      label: 'Option 1'
    }, {
      value: 'Option 2',
      label: 'Option 2'
    }, {
      value: 'Option 3',
      label: 'Option 3'
    }, {
      value: 'Option 4',
      label: 'Option 4'
    }, {
      value: 'Option 5',
      label: 'Option 5'
    }],
    value: ''
  };
    render() {
        return (
            <div className="navbar">
                <span className="navbar__elem1">
                    <a href="?" className="navbar__brand">
                        <span className="navbar__brand__logo"></span>
                        <span className="navbar__brand__name">WAKECAP</span>
                    </a>
                    <div className="navbar__info">
                        <div className="navbar__info__elem1">WORKERS</div>
                        <div className="navbar__info__elem2">Overview</div>
                    </div>
                </span>
                <span className="navbar__elem2">
                    <Select placeholder="Select site" className="navbar__user-site" value={this.state.value} clearable={true}> {
                        this.state.options.map(el => {
                        return <Select.Option key={el.value} label={el.label} value={el.value} />})}
                    </Select>
                    <Dropdown trigger="click" menu={(
                        <Dropdown.Menu>
                            <Dropdown.Item>Action 1</Dropdown.Item>
                            <Dropdown.Item>Action 2</Dropdown.Item>
                            <Dropdown.Item>Action 3</Dropdown.Item>
                            <Dropdown.Item>Action 4</Dropdown.Item>
                            <Dropdown.Item>Action 5</Dropdown.Item>
                        </Dropdown.Menu>)}>
                        <Button size="small" type="info">
                            <span className="navbar__user-avatar"><i className="far fa-user-circle"></i></span>
                            <span>Username</span>
                            <i className="el-icon-caret-bottom el-icon--right"></i>
                        </Button>
                    </Dropdown>
                </span>
            </div>
        );
    }
}
