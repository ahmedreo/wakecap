import React, { Component } from 'react';
import NavBar from '../components/NavBar';
import SideBar from '../components/SideBar';
import Workers from '../components/Workers';
import WorkerInfo from '../components/WorkerInfo';
import WorkerActivity from '../components/WorkerActivity';
import Helmets from '../components/Helmets';
import '../sass/home.scss';

export default class Home extends Component {
	render() {
		return (
			<div className="home">
				<NavBar/>
				<div className="home__content flex grow height-blocked">
					<SideBar/>
					<div className="home__content__elem1 flex grow width-blocked">
						<Workers/>
						<div className="home__content__elem2 grow width-blocked"> 
							<WorkerInfo/>
							<WorkerActivity/>
							<Helmets/>
						</div>
					</div>
				</div>
			</div>
		);
	}
}